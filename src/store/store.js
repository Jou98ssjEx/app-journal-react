import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import { authReducer } from '../reducers/authReducer';
import thunk from 'redux-thunk'
import { uiReducer } from '../reducers/uiReducer';
import { noteReducer } from '../reducers/noteReducer';

// configuracion de las devTools de redux
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;


// aqui add cualquier reducer
const reducers = combineReducers({
    auth: authReducer,
    ui: uiReducer,
    notes: noteReducer,
});

// el corazon de redux: el stare
export const store = createStore(
    reducers,
    composeEnhancers(
        // aplicar middleware para las acciones asincronas
        applyMiddleware(thunk)
    )
);