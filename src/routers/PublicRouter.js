import React from 'react'
import { Navigate } from 'react-router-dom'

export const PublicRouter = ({children, isLoggedIn}) => {
    // console.log(isLoggedIn)
    //  isLoggedIn ? <Navigate to={'/'}/>: <Navigate to={'/auth/login'}/>;
    return !isLoggedIn ? children : <Navigate to={'/'}/>
    // return children
}
