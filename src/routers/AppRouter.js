import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { JournalScreen } from '../components/journal/JournalScreen'
import { AuthRouter } from './AuthRouter'

import {firebase} from '../firebase/firebase-config'
import { login } from '../actions/auth'
import { PublicRouter } from './PublicRouter'
import { PrivateRouter } from './PrivateRouter'
// import { loadNotes } from '../helpers/loadNotes'
import {  startLoadingNotes } from '../actions/notes'

export const AppRouter = () => {

    const dispatch = useDispatch();

    const [cheking, setCheking] = useState(true);

    const [isLoggedIn, setIsLoggedIn] = useState(false);
    // console.log(isLoggedIn)

    useEffect(() => {
        
        // Mantener la authenticacion del usuario
        firebase.auth().onAuthStateChanged(async (user) => {
            // console.log(user)
            if (user?.uid){
                dispatch(login(user.uid, user.displayName))

                // saber si estoy logeado
                setIsLoggedIn(true);

                // // cargar notas del usuario
                // const notes =  await loadNotes(user.uid);

                dispatch(startLoadingNotes(user.uid));
            } else{
                setIsLoggedIn(false);
            }
        })

        setCheking(false);
       
    }, [dispatch, setCheking, setIsLoggedIn])

    if(cheking){
        return(
            <h1>Waiting....</h1>
        )
    }

    return (
       <BrowserRouter>
            <Routes>
                {/* <Route path='/auth/*' element={ <AuthRouter/>} />

                <Route path='/' element={ <JournalScreen />} /> */}

                {/* ruta publica */}
                <Route path='/auth/*' element={
                    // !isLoggedIn &&
                    <PublicRouter isLoggedIn={isLoggedIn}>
                        <AuthRouter/>
                    </PublicRouter>
                } />

                {/* // rutas privadas */}
                <Route path='/' element={
                    // isLoggedIn &&
                    <PrivateRouter isLoggedIn={isLoggedIn}>
                        <JournalScreen />
                    </PrivateRouter>
                } />
{/* 
                <Route path="*" element={
                     <PublicRouter isLoggedIn={isLoggedIn}>
                     <AuthRouter/>
                 </PublicRouter>
                } /> */}

                {/* <Route
                    path="*"
                    element={<Navigate to="/" />}
                /> */}

            </Routes>
    
       </BrowserRouter>
    )
}
