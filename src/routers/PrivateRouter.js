import React from 'react'
import { Navigate } from 'react-router-dom'

export const PrivateRouter = ({children, isLoggedIn}) => {
    // return isLoggedIn ? children : <Navigate to={'/auth/login'}/>
    return isLoggedIn ? children: <Navigate to={'/auth/login'}/>;

    // return children


}
