// import firebase from 'firebase/app';
// import 'firebase/firestore';
// import 'firebase/auth';

// v9 compat packages are API compatible with v8 code
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';


const firebaseConfig ={
    apiKey: "AIzaSyAcoTiwpLupr79Xvi3VlW9CumTZzn9LRpI",
    authDomain: "react-app-faf56.firebaseapp.com",
    projectId: "react-app-faf56",
    storageBucket: "react-app-faf56.appspot.com",
    messagingSenderId: "817116552677",
    appId: "1:817116552677:web:53f3c41e20f8f09a5de30c",
    measurementId: "G-YKGN7L78BR"
}


// v antigua
// firebase.initializeApp(firebaseConfig);
// const db = firebase.firestore();
// const googleAuthProvider = new firebase.auth.GoogleAuthProvider();


firebase.initializeApp(firebaseConfig);
const db = firebase.firestore(); // lleva la app
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();


export{
    db,
    googleAuthProvider,
    firebase
}