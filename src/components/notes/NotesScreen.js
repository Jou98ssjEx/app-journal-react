import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from '../../hooks/useForm';
import { NotesAppBar } from './NotesAppBar'

import {activeNote, startDeleteNote} from '../../actions/notes'

export const NotesScreen = () => {

    const dispatch = useDispatch();

    const {active:note} = useSelector(state => state.notes);

    const [formValues, handleInputChange, reset] = useForm(note);
    // console.log(formValues)
    // si saco la url directamente de aqui no va a notar el cambio xq viene underfine
    const { title, body } = formValues;
    // console.log({url});

    // tener la referencia del id de la nota cuando cambia
    const activeId = useRef();

    useEffect(() => {
        if(note.id !== activeId.current){

            // cambie el valor de la nota con el reset
            reset(note);
            // cambie el id
            activeId.current = note.id
        }
    }, [reset, note])

    // cambio en los input
    useEffect(() => {

        // dispatch()
        dispatch(activeNote(formValues.id, { ...formValues }))
    }, [dispatch, formValues]);

    // eliminar nota
    const handleDelete = ( ) => {
        // console.log('note delete');
        dispatch( startDeleteNote(note.id))
    }

    return (
        <div className='notes__main-content'>

            <NotesAppBar />
            <div className='notes__content'>

                {/* <form> */}
                    <input
                        type='text'
                        placeholder='Some awesome title'
                        className='notes__title-input'
                        name='title'
                        value={title}
                        onChange={handleInputChange}
                    />

                    <textarea
                        placeholder='what happend today'
                        className='notes__textaere'
                        name='body'
                        value={body}
                        onChange={handleInputChange}
                    >
                    </textarea>

                    {
                        note.url && 
                        (
                            <div className='notes__image'>
                                <img
                                    // src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSlPyxX2_L4-MByGH2eP3hVSaDSFsTIZe--4Q&usqp=CAU'
                                    // src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVp9iHEh1ve3HevLRaLY1WOYVRYRp3ti26Qw&usqp=CAU'
                                    src={note.url}
                                    alt='img'
                                />
                            </div>
                        )
                    }

                {/* </form> */}

            </div>

            <button
                className='btn btn-danger'
                onClick={ handleDelete }
            >
                Delete note
            </button>
        </div>
    )
}
