import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startFileUpload, startSaveNote } from '../../actions/notes'

export const NotesAppBar = () => {

    const dispatch = useDispatch()
    const {active} = useSelector(state => state.notes)   
    const handleSave = () => {

        dispatch( startSaveNote(active));
    }

    const handlePicture = () => {
        console.log('click');
        document.querySelector('#inputFile').click();
    }

    const handleFileChange = ( e ) => {
        // console.log(e.target.files[0]);
        const file = e.target.files[0]

        // action
        if(file){
            // file Upload
            dispatch( startFileUpload(file));
        }
    }

    return (
        <div className='notes__appbar'>
            <span>28 de agosto 2022</span>

            <input
                id='inputFile'
                type='file'
                onChange={ handleFileChange }
                style={ { display:'none' }}
            />
            <div>
                <button
                    onClick={ handlePicture }
                    className='btn'
                >
                    Picture
                </button>
                <button 
                    className='btn'
                    onClick={handleSave}
                >
                    Save
                </button>
            </div>
        </div>
    )
}
