import React from 'react'
import { useSelector } from 'react-redux'
import { NotesScreen } from '../notes/NotesScreen'
import { Siderbar } from './Siderbar'
import { NothingSelected } from './NothingSelected'

export const JournalScreen = () => {


    const {active} = useSelector(state => state.notes);   

    return (
        <div
            className='journal__main-content'
        >

            <Siderbar/>
            <main>

                {
                    (active)
                        ? ( <NotesScreen/>)
                        : ( <NothingSelected />)
                }
                
                {/* <NotesScreen /> */}
                {/* <NothingSelected/> */}
            </main>
        </div>
    )
}
