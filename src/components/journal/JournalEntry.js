import React from 'react'
import moment from 'moment'
import { useDispatch } from 'react-redux';
import { activeNote } from '../../actions/notes';
export const JournalEntry = ( {title, body, date, url, id} ) => {
    // console.log(body)
    const dateRef = moment(date);

    const dispatch = useDispatch();

    const handleEntryClick = ( ) => {
        // console.log('1')

        dispatch(activeNote(id, {title, body, date, url}))
    }

    return (
        <div 
            className='journal_entry pointer'
            onClick={handleEntryClick}
        >
            {
                // cargar si existe la img
                url &&
                (
                    <div 
                        className='journal_entry-picture'
                        style={{
                            backgroundSize: 'cover',
                            backgroundImage: `url(${url})`
                        }}
                    >  </div>
                )
            }

            <div className='journal__entry-body'>
                <p className='journal__entry-title'>
                    {title}
                </p>
                <p className='journal__entry-content'>
                    {body}
                </p>
            </div>

            <div className='journal__entry-date-box'>
                <span> {dateRef.format('dddd')} </span>
                <h4>{dateRef.format('Do')}</h4>

            </div>
            {/* <h4>Journal entry</h4> */}
        </div>
    )
}
