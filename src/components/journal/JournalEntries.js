import React from 'react'
import { useSelector } from 'react-redux';
import { JournalEntry } from './JournalEntry';

export const JournalEntries = () => {

    // const entries = [1,2,3,4];

    const {notes} = useSelector(state => state.notes)

    return (
        <div className='journal__entries'>

            {
                // entries.map( value => (
                //     <JournalEntry key={value} />
                // ))
                notes.map( note => (
                    // console.log(data),
                    <JournalEntry 
                        key={note.id}
                        {...note}
                    />
                ))
            }
            
        </div>
    )
}
