import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { startGoogleLogin, startLoginEmailPassword } from '../../actions/auth'
import { useForm } from '../../hooks/useForm'

export const LoginScreen = () => {

    const dispatch = useDispatch();
    // useSelector
    const {logged} = useSelector(state => state.ui);
    // console.log(logged)

    const [ formValues, hadleChangeInput  ] = useForm({
        email: 'barry12@gmail.com',
        password: 'flash12'
    });

    
    const { email, password } = formValues;
    
    const handleLogin = (e) =>{
        e.preventDefault();
        // console.log(email)
        dispatch( startLoginEmailPassword(email, password))

        // if ( isFormValid() ){
        //     console.log('Form successfy');
        //     dispatch(startRegisterNameEmailPassword(name, email, password))
        // } 
    }

    const handleGoogleLogin = ( ) => {
        dispatch( startGoogleLogin() );
    }

    // const isFormValid = () => {

    //     if( name.trim().length === 0){
    //         console.log('Name is required')
    //         dispatch( setError('Name is required') )
    //         return false
    //     } else if (!validator.isEmail(email)){
    //         console.log('Email incorrect')
    //         dispatch( setError('Email incorrect') )
    //         return false
    //     } else if ( password !== confirm || password.length < 4){
    //         console.log('Error in password')
    //         dispatch( setError('Error in password') )
    //         return false
    //     }

    //     dispatch( removeError());
        
    //     return true
    // }

    return (
        <div>
            <h3 className='auth__title'>Login</h3>
            <form onSubmit={handleLogin}>
                <input 
                    type='text'
                    placeholder='email'
                    name='email'
                    className='auth__input'
                    autoComplete='off'
                    value={email}
                    onChange={hadleChangeInput}
                />
                <input 
                    type='password'
                    placeholder='password'
                    name='password'
                    className='auth__input'
                    value={password}
                    onChange={hadleChangeInput}
                />
                <button
                    className='btn btn-primary btn-block'
                    type='submit'
                    disabled={logged}
                >
                    Login
                </button>

                <div className='auth__social-networks'>
                    <p>Login with social networks</p>
                    <div 
                        className="google-btn"
                        onClick={handleGoogleLogin}
                    >
                        <div className="google-icon-wrapper">
                            <img className="google-icon" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg" alt="google button" />
                        </div>
                        <p className="btn-text">
                            <b>Sign in with google</b>
                        </p>
                    </div>
                </div>

                

                <Link 
                    to='/auth/register'
                    className='link mt-1'
                >
                    Create new account
                </Link>
            </form>
        </div>
    )
}
