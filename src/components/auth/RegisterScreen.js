import React from 'react'
import { Link } from 'react-router-dom'
import { useForm } from '../../hooks/useForm'
import validator from 'validator'
import { useDispatch, useSelector } from 'react-redux'
// import { uiReducer } from '../../reducers/uiReducer'
import { removeError, setError } from '../../actions/ui'
import { startRegisterNameEmailPassword } from '../../actions/auth'

export const RegisterScreen = () => {

    // acciones de los reducer
    const dispatch = useDispatch();

    // consigo el stado del los errores: ui
    const {msgError} = useSelector(state => state.ui)

    const [ formValues, handleInputChange ] = useForm({
        name: 'Barry Allen',
        email: 'barry12@gmail.com',
        password: 'flash12',
        confirm: 'flash12'
    });

    const { name, email, password, confirm } = formValues;

    const handleRegister = ( e ) => {
        e.preventDefault();
        // console.log({name , email , password , confirm})
        if ( isFormValid() ){
            console.log('Form successfy');
            dispatch(startRegisterNameEmailPassword(name, email, password))
        } 
    } 


    const isFormValid = () => {

        if( name.trim().length === 0){
            console.log('Name is required')
            dispatch( setError('Name is required') )
            return false
        } else if (!validator.isEmail(email)){
            console.log('Email incorrect')
            dispatch( setError('Email incorrect') )
            return false
        } else if ( password !== confirm || password.length < 4){
            console.log('Error in password')
            dispatch( setError('Error in password') )
            return false
        }

        dispatch( removeError());
        
        return true
    }

    return (
        <div>
            <h3 className='auth__title'>Register</h3>
            <form onSubmit={handleRegister}>
                {
                    msgError && (
                        <div className='auth__alert-error'>
                            {msgError}
                        </div>  
                    )
                }
               
                <input 
                    type='text'
                    placeholder='Name'
                    name='name'
                    className='auth__input'
                    autoComplete='off'
                    value={name}
                    onChange={handleInputChange}
                />
                <input 
                    type='text'
                    placeholder='Email'
                    name='email'
                    className='auth__input'
                    autoComplete='off'
                    value={email}
                    onChange={handleInputChange}
                />
                <input 
                    type='password'
                    placeholder='Password'
                    name='password'
                    className='auth__input'
                    value={password}
                    onChange={handleInputChange}
                />
                <input 
                    type='password'
                    placeholder='Confirm'
                    name='confirm'
                    className='auth__input'
                    value={confirm}
                    onChange={handleInputChange}
                />
                <button
                    className='btn btn-primary btn-block mb-5'
                    type='submit'
                >
                    Register
                </button>

               

                <Link 
                    to='/auth/login'
                    className='link mt-1'
                >
                    Alredy resgister?
                </Link>
            </form>
        </div>
    )
}
