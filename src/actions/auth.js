import Swal from 'sweetalert2'
import { firebase, googleAuthProvider } from "../firebase/firebase-config";
import { types } from "../types/types";
import { logoutCleaningNote } from './notes';
import { finishLoading, startLoading } from "./ui";

export const startLoginEmailPassword = ( email, password) => {
    return async(dispatch) => {


        try {
            dispatch(startLoading());
            
            // auth a firebase
            const {user} = await firebase.auth().signInWithEmailAndPassword(email, password);
            // console.log(user);

            dispatch(login(user.uid, user.displayName))
            dispatch(finishLoading());

        } catch (error) {
            dispatch(finishLoading());
            // console.log(error.message)
            Swal.fire('Error', error.message, 'error')
        }
    
    }
}


export const startRegisterNameEmailPassword = ( name, email, password) => {
    return (dispatch) => {

        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then( async ( {user}) =>{
                await user.updateProfile({displayName: name});

                dispatch(
                    login(user.uid, user.displayName)
                )
            } )
            .catch( e=>console.log)
     }
}

// export const startRegisterNameEmailPassword = ( name, email, password) => {
//     return async(dispatch) => {

//         const {user} = await firebase.auth().createUserWithEmailAndPassword(email, password);
//         await user.updateProfile({displayName: name});

//         dispatch( login(user.uid, user.displayName));


//     }
// }

export const startGoogleLogin = () => {
    return (dispatch) => {
        dispatch(startLoading());

        firebase.auth().signInWithPopup(googleAuthProvider)
            .then(({user}) => {
                // console.log(user)
                dispatch( login(user.uid, user.displayName))

                dispatch(finishLoading());

            })
            .catch(e =>console.log)
    }
}

export const login = (uid, displayName) =>({
    type: types.login,
    payload:{
        uid,
        displayName
    }
})


export const startLogout = ( ) => {
    return async (dispatch ) => {

        // cerrar cesion
        await firebase.auth().signOut();

        dispatch(logout())

        // limpiar las notas
        dispatch( logoutCleaningNote())
    }
}

export const logout = () => ({
    type: types.logout
})